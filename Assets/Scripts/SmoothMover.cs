﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class SmoothMover : MonoBehaviour, ITarget
    {
        private const float RotationThreshold = 0.0001f;
        private const float AccelerationRequiredForAtan = 0.05f;

        public float Speed;
        public float RotationSpeed;
        public float MaxRotationSpeed;
        public float MaxAcceleration;
        public float MinSpeed;
        public float MaxSpeed;

        public float BlindRadius { get { return Speed/MaxRotationSpeed; } }
        // all next fields are cached to avoid multiple native calls in one fixed update frame
        public Vector2 Position { get; private set; }
        public Vector2 GetFuturePosition(float time)
        {
            Vector2 futurePos = Position;
            if (!IsMoving)
            {
                // movement without rotation
                futurePos += Velocity * time;
                return futurePos;
            }
            // moving in circle
            float rotationRadius = Speed/RotationSpeed;
            float angle = time*RotationSpeed;
            Vector2 circleCenter = futurePos + Left*rotationRadius;
            Vector2 circlePos = -Left*rotationRadius;
            Vector2 newCirclePos = circlePos.Rotate(angle);
            futurePos = circleCenter + newCirclePos;
            return futurePos;
        }
        public bool IsMoving { get { return Speed > 0.01f; } }
        public bool IsRotating { get { return Mathf.Abs(RotationSpeed) > RotationThreshold; } }

        public Vector2 Forward { get; private set; }
        public Vector2 Left { get; private set; }
        public float Rotation { get; private set; }
        public Vector2 Velocity { get; private set; }

        private Rigidbody _rigidbody;
        private Transform _transform;
        private ISmoothMoveController[] _controllers;
#if UNITY_EDITOR    // for debug
        private Vector2[] _controllersImpacts;
#endif

        void Awake()
        {
            Speed = MinSpeed;
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.angularDrag = 0f;
            _rigidbody.drag = 0f;
            _rigidbody.useGravity = false;
            _controllers = GetComponents<ISmoothMoveController>();
#if UNITY_EDITOR
            _controllersImpacts = new Vector2[_controllers.Length];
#endif
        }
        
        void FixedUpdate()
        {
            UpdateCache();
            UpdateControllers();
            Move();
        }

        void OnEnable()
        {
            _rigidbody.isKinematic = false;
        }

        void OnDisable()
        {
            _rigidbody.isKinematic = true;
        }

        private void UpdateCache()
        {
            Position = _transform.position;
            Forward = _transform.up;
            Left = new Vector2(-Forward.y, Forward.x);
            Rotation = _transform.rotation.eulerAngles.z*Mathf.Deg2Rad;
            Velocity = _rigidbody.velocity;
        }

        private void UpdateControllers()
        {
            Vector2 force = Vector2.zero;
            for (int i = 0; i < _controllers.Length; i++)
            {
                if (_controllers[i].Equals(null) // component was destroyed in Unity
                    || !_controllers[i].Enabled)
                {
#if UNITY_EDITOR
                    _controllersImpacts[i] = Vector2.zero;
#endif
                    continue;
                }
                Vector2 currentTarget = _controllers[i].ControlMovement(this);
                force += currentTarget;
#if UNITY_EDITOR
                _controllersImpacts[i] = currentTarget;
#endif
            }
            // convert the force vector into acceleration
            float acceleration = Vector2.Dot(force, Forward);
            acceleration = Mathf.Clamp(acceleration, -MaxAcceleration, MaxAcceleration);
            Speed = Mathf.Clamp(Speed + acceleration * Time.fixedDeltaTime, MinSpeed, MaxSpeed);
            // convert the force vector into angular speed
            if (force.magnitude > float.Epsilon)
            {
                float relativeRotation; // use only projection on left axis for gradual rotations when there is no acceleration (mainly for input controller)
                if (Mathf.Abs(acceleration) > AccelerationRequiredForAtan)
                {
                    relativeRotation = Mathf.Atan2(-force.x, force.y);
                    relativeRotation -= Rotation;
                    // represent rotation in a [-pi, pi] range
                    while (relativeRotation < -Mathf.PI)
                        relativeRotation += 2 * Mathf.PI;
                    while (relativeRotation > Mathf.PI)
                        relativeRotation -= 2 * Mathf.PI;
                }
                else relativeRotation = RotationSpeed + Vector2.Dot(force, Left);
                RotationSpeed = Mathf.Clamp(relativeRotation, -MaxRotationSpeed, MaxRotationSpeed);
            }
        }

        // must be executed after all other controlling components' FixedUpdates
        private void Move()
        {
            Vector2 currentVelocity = _rigidbody.velocity;
            float dt = Time.fixedDeltaTime;
            Vector2 desiredVelocity = Forward;
            // curve a trajectory
            float rotationAngle = RotationSpeed*dt;
            desiredVelocity = desiredVelocity.Rotate(rotationAngle);
            // make the object rotate with the desired rotation speed
            desiredVelocity.Normalize();
            float desiredRotationSpeed = Mathf.Asin(Forward.x*desiredVelocity.y - Forward.y*desiredVelocity.x)*Mathf.Rad2Deg;
            float currentRotationSpeed = _rigidbody.angularVelocity.z;
            _rigidbody.AddTorque(0f, 0f, (desiredRotationSpeed - currentRotationSpeed)/dt, ForceMode.Acceleration);
            // set correct object speed
            desiredVelocity = desiredVelocity*Speed;
            Vector2 f = (desiredVelocity - currentVelocity)/dt;
            _rigidbody.AddForce(f, ForceMode.Acceleration);
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            if (!enabled) return;
            for (int i = 0; i < _controllersImpacts.Length; i++)
            {
                Gizmos.color = new Color(i % 3 == 0 ? 1f : 0f, i % 3 == 1 ? 1f : 0f, i % 3 == 2 ? 1f : 0f);
                Gizmos.DrawLine(Position, Position + _controllersImpacts[i] * 3f);
            }
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(Position + BlindRadius * Left, BlindRadius);
            Gizmos.DrawWireSphere(Position - BlindRadius * Left, BlindRadius);
        }
#endif
    }
}
