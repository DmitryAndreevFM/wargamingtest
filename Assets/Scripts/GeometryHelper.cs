﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class GeometryHelper
    {
        public static Vector2 Rotate(this Vector2 v, float radians)
        {
            float sina = Mathf.Sin(radians);
            float cosa = Mathf.Cos(radians);
            float previousX = v.x;
            v.x = cosa * previousX - sina * v.y;
            v.y = sina * previousX + cosa * v.y;
            return v;
        }
    }
}
