﻿using UnityEngine;

namespace Assets.Scripts
{
    public class StaticTarget : ITarget
    {
        private readonly Vector2 _position;

        public StaticTarget(Vector2 position)
        {
            _position = position;
        }

        public Vector2 Position
        {
            get { return _position; }
        }

        public Vector2 GetFuturePosition(float time)
        {
            return _position;
        }

        public bool IsMoving
        {
            get { return false; }
        }

        public bool IsRotating
        {
            get { return false; }
        }
    }
}
