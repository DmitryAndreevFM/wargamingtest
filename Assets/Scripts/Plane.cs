﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(DistanceController))]
    [RequireComponent(typeof(TargetCatcher))]
    public class Plane : MonoBehaviour, IPlane
    {
        public SmoothMover Mover { get; private set; }
        public float Fuel { get; set; }
        public float FuelPerSecond = 1f;

        private TargetCatcher _targetCatcher;
        private DistanceController _distanceController;
        private IAirTrafficController _airTrafficController;
        private Transform _transform;

        void Awake()
        {
            Mover = GetComponent<SmoothMover>();
            _targetCatcher = GetComponent<TargetCatcher>();
            _targetCatcher.TargetReached += OnTargetReached;
            _targetCatcher.PathCompleted += OnPathCompleted;
            _transform = transform;
            _distanceController = GetComponent<DistanceController>();
        }

        void Update() // todo: optimization, maybe coroutines
        {
            Fuel -= Time.deltaTime*FuelPerSecond;
            if (Fuel < 0f
                && !float.IsNegativeInfinity(Fuel)) // todo: more elegant logic. teoretically we'll fall
            {
                AbortMission();
                Fuel = float.NegativeInfinity;
            }
        }

        public void StartMission(List<ITarget> targets)
        {
            _targetCatcher.ClearPath();
            _distanceController.Enabled = true;
            int count = targets.Count;
            for (int i = 0; i < count; i++)
                _targetCatcher.AppendPath(targets[i]);
        }

        public void KeepDistance(List<ITarget> targets, float distance)
        {
            int count = targets.Count;
            for (int i = 0; i < count; i++)
                _distanceController.AddTarget(targets[i], distance);
        }

        public void AbortMission()
        {
            _targetCatcher.ClearPath();
            if (_airTrafficController != null)
                _targetCatcher.AppendPath(_airTrafficController.GetAirportTarget());
            _distanceController.Enabled = false;
        }

        public void AttachToAirport(IAirTrafficController airTrafficController)
        {
            _airTrafficController = airTrafficController;
        }

        public void TakeOff()
        {
            _transform.parent = null;
            _targetCatcher.Enabled = true;
            Mover.enabled = true;
            Landed = false;
        }

        public void TouchDown(Transform airportTransform)
        {
            _transform.parent = airportTransform;
            _distanceController.ForgetAllTargets();
            _targetCatcher.Enabled = false;
            Mover.enabled = false;
            Landed = true;
        }

        public bool Landed { get; private set; }

        private void OnTargetReached(ITarget target)
        {
            if (target == _airTrafficController.GetAirportTarget())
                _airTrafficController.TryLand(this);
        }

        private void OnPathCompleted()
        {
            AbortMission();
        }
    }
}
