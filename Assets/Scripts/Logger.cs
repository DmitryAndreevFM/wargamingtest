﻿using UnityEngine;

namespace Assets.Scripts
{
    public interface ILogger
    {
        void Say(string message);
        void Say(int importance, string message);
        void Say(int importance, string format, params object[] args);
    }

    public class Logger : ILogger
    {
        
        public void Say(string message)
        {
            Say(0, message);
        }

        public void Say(int importance, string message)
        {
            string ms = message + "\n";
            switch (importance)
            {
                case 0: Debug.Log(ms); break;
                case 1: Debug.LogWarning(ms); break;
                case 2: Debug.LogError(ms); break;
            }
        }

        public void Say(int importance, string format, params object[] args)
        {
            string ms = format + "\n";
            switch (importance)
            {
                case 0: Debug.LogFormat(ms, args); break;
                case 1: Debug.LogWarningFormat(ms, args); break;
                case 2: Debug.LogErrorFormat(ms, args); break;
            }
        }
    }
}
