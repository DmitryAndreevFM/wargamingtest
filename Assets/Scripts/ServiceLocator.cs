﻿namespace Assets.Scripts
{
    public static class ServiceLocator
    {
        public static ILogger Logger;

        static ServiceLocator()
        {
            // generate fake services
            Logger = new FakeLogger();
        }

        private class FakeLogger : ILogger
        {
            public void Say(string message) { }
            public void Say(int importance, string message) { }
            public void Say(int importance, string format, params object[] args) { }
        }
    }
}
