﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class TargetCatcher : MonoBehaviour, ISmoothMoveController
    {
        private const int PredictionIterations = 10;
        private const float PredictedTimeGiveupFactor = 1.8f;
        private const float PositionAccuracyThreshold = 0.1f;
        private const float BlindZoneSubzonesFactor = 0.8f;
        private const float NoAccelerationTime = 1f;

        public float TargetRadius = 1f;
        [Range(0f, 2f)]
        public float Impact = 1f;
        public ITarget Target { get; set; }
        private bool _enabled;
        public bool Enabled { get { return _enabled; } set { _enabled = value; enabled = _enabled; } }

        public event Action<ITarget> TargetReached;
        public event Action PathCompleted;

        private Queue<ITarget> _path;

        // todo: for debug puposes only
        private Vector2 _lastPrediction;

        void Awake()
        {
            _enabled = enabled;
            if (_path == null)
                _path = new Queue<ITarget>();
        }

#if UNITY_EDITOR
        // to be able to disable the component in runtime in the editor
        void Update() { }
        void OnEnable() { _enabled = true; }
        void OnDisable() { _enabled = false; }
#endif

        public void AppendPath(ITarget target)
        {
            Awake(); // todo: guarantee that _path already initialyzed before IAirTrafficController.Awake
            if (Target == null)
                Target = target;
            else
                _path.Enqueue(target);
        }

        public void ClearPath()
        {
            Awake(); // todo: guarantee that _path already initialyzed before IAirTrafficController.Awake
            Target = null;
            _path.Clear();
        }

        public Vector2 ControlMovement(SmoothMover mover)
        {
            Vector2 force = Vector2.zero;
            Vector2 toTarget = Target == null ? Vector2.zero : mover.Position - Target.Position;
            if (toTarget.sqrMagnitude < TargetRadius * TargetRadius)
            {
                if (TargetReached != null && Target != null)
                    TargetReached(Target);
                // get next target
                if (_path.Count != 0)
                    Target = _path.Dequeue();
                else
                    if (PathCompleted != null)
                        PathCompleted();
                if (Target != null)
                    force = CalculateForceForMovement(mover);
            }
            else
            {
                // todo: maybe call not in each FixedUpdate? nothing changed unless the target changed course
                if (Target != null)
                    force = CalculateForceForMovement(mover);
            }
            force.Normalize();
            return force*Impact;
        }

        private Vector2 CalculateForceForMovement(SmoothMover mover)
        {
            if (!mover.IsMoving)
                return Vector2.zero; // we are unable to catch the target
            _lastPrediction = Target.Position;
            Vector2 force;
            float noPredictionTime = EstimateCatchPathAndTime(Target.Position, mover, out force);
            if (float.IsInfinity(noPredictionTime))
                return Vector2.zero;
            if (!Target.IsMoving)
                return force; // no need for predictions - target is static
            // now make iterative predictions based on the time that was calculated previously
            float lastTime = noPredictionTime;
            for (int i = 0; i < PredictionIterations; i++)
            {
                Vector2 prediction = Target.GetFuturePosition(lastTime);
                _lastPrediction = prediction;
                lastTime = EstimateCatchPathAndTime(prediction, mover, out force);
                if (lastTime < NoAccelerationTime) // disable acceleration right before we hit target to avoid miss
                    force -= Vector2.Dot(mover.Forward, force) * mover.Forward;
                // prevent further calculations if time increasing (seems like the speed of target
                // exceeds our speed)
                if (lastTime > noPredictionTime * PredictedTimeGiveupFactor)
                    break;
                // todo: compare with previous time and detect convergence for preliminary break
            }
            return force;
        }

        private float EstimateCatchPathAndTime(Vector2 targetPos, SmoothMover mover, out Vector2 force)
        {
            float rotation = mover.Rotation;
            // calculate target position in local space
            Vector2 moverPos = mover.Position;
            Vector2 targetLocal = targetPos - moverPos;
            targetLocal = targetLocal.Rotate(-rotation);
            // determine if the target is in a blind zone
            float blindRadius = mover.BlindRadius;
            if (float.IsInfinity(blindRadius))
            {
                force = Vector2.zero;
                return float.PositiveInfinity; // we are unable to rotate
            }
            float absX = Mathf.Abs(targetLocal.x);
            if (targetLocal.y > 0f && absX < PositionAccuracyThreshold)
            {
                // we are flying to the target in a straight line
                force = targetPos - moverPos;
                float time = targetLocal.y/mover.Speed;
                return time;
            }
            // now find a blind zone radius that is guarantees the target will be on a blind zone border
            float tx = targetLocal.x;
            float ty = targetLocal.y;
            float currentRadius = Mathf.Abs(tx + ty * ty / tx) * 0.5f;
            float minBlindRadius = mover.MinSpeed/mover.MaxRotationSpeed;
            if (currentRadius > blindRadius / BlindZoneSubzonesFactor)
            {
                // it's allowed to gain acceleration - the target is far from blind zone
                force = targetPos - moverPos;
                // approximate time as a linear movement across the straight line
                // (the curvature increases the time, but acceleration - decreases)
                float time = targetLocal.magnitude / mover.Speed;
                return time;
            }
            if (currentRadius < minBlindRadius)
            {
                // the target is inside a minimal possible blind zone - no way we'll hit it
                // by simply following it. We need to move away from target to get more space
                force = tx > 0 ? mover.Left : -mover.Left;
                force -= mover.Forward; // it's better to slow down
                // approximate time as a time needed for full circle movement w/o last line
                float time = (2f * Mathf.PI * minBlindRadius - targetLocal.magnitude) / mover.Speed;
                return time;
            }
            {
                // the target is in the blind zone, but we can slow down and hit it
                // OR the target is near (but outside) a blind zone - acceleration is forbidden! (but it's better to slow down)
                force = tx > 0 ? -mover.Left : mover.Left;
                force -= mover.Forward; // we have to slow down
                // approximate the time we'll be in the target position in.
                float blindDisplacementX = absX - minBlindRadius;
                float fullBlindAngle = Mathf.Atan2(ty, -blindDisplacementX);
                float time = Mathf.Abs(fullBlindAngle)/mover.MaxRotationSpeed;
                return time;
            }
        }

        void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying
                || !Enabled || Target == null) return;
            Gizmos.color = Color.red;
            Vector2 target = Target.Position;
            Gizmos.DrawSphere(new Vector3(target.x, target.y), 0.1f);
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(new Vector3(_lastPrediction.x, _lastPrediction.y), 0.12f);
            if (_path.Count == 0)
                return;
            Gizmos.color = Color.green;
            foreach (ITarget t in _path)
            {
                target = t.Position;
                Gizmos.DrawSphere(new Vector3(target.x, target.y), 0.1f);
            }
        }
    }
}
