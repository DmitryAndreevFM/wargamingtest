﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScoutTrafficController : MonoBehaviour, IAirTrafficController
    {
        public int TargetsInMission = 5;
        public float TargetsDistance = 9f;
        public float PlaneFuelPerMission = 20f;
        public GameObject[] Planes;
        public float LandingDistance = 1f;
        public float PlaneDistance = 8f;

        private List<IPlane> _planes;
        private ITarget _airport;
        private Transform _transform;
        private IPlane _lastPlane;

        void Start()
        {
            _airport = GetComponent<ITarget>();
            _transform = transform;
            _planes = new List<IPlane>(Planes.Length);
            for (int i = 0; i < Planes.Length; i++)
            {
                IPlane plane = Planes[i].GetComponent<IPlane>();
                if (plane == null || plane.Mover == null) continue;
                _planes.Add(plane);
                plane.AttachToAirport(this);
                plane.KeepDistance(new List<ITarget>(1) { _airport }, PlaneDistance);
                Vector2 initialDistance = plane.Mover.Position;
                initialDistance -= _airport.Position;
                if (initialDistance.sqrMagnitude < LandingDistance * LandingDistance)
                    plane.TouchDown(_transform);
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.H))
                TrySendScout();
            if (Input.GetKeyDown(KeyCode.S))
                TrySendScout(true);
        }

        public ITarget GetAirportTarget()
        {
            return _airport;
        }

        public bool TryLand(IPlane plane)
        {
            // do not allow to land plane that is not attached to this airport
            if (!_planes.Contains(plane)) // todo: optimization
                return false;
            if ((plane.Mover.Position - _airport.Position).sqrMagnitude > LandingDistance*LandingDistance)
                return false;
            plane.TouchDown(_transform);
            if (_lastPlane == plane)
                _lastPlane = null;
            return true;
        }

        public bool TrySendScout(bool resend = false)
        {
            IPlane plane = _planes.FirstOrDefault(p => p.Landed);
            if (plane == null)
            {
                if (!resend || _planes.Count == 0)
                    return false;
                plane = _planes[0];
            }
            if (plane.Landed)
            {
                plane.Fuel = PlaneFuelPerMission;
                plane.TakeOff();
            }
            plane.StartMission(GenerateTargetList());
            plane.KeepDistance(
                _lastPlane == null
                    ? new List<ITarget>(1) {_airport}
                    : new List<ITarget>(2) {_airport, _lastPlane.Mover},
                PlaneDistance);
            _lastPlane = plane;
            return true;
        }

        private List<ITarget> GenerateTargetList()
        {
            List<ITarget> result = new List<ITarget>(TargetsInMission);
            for (int i = 0; i < TargetsInMission; i++)
            {
                Vector2 position = _airport.Position + Random.insideUnitCircle*TargetsDistance;
                ITarget t = new StaticTarget(position);
                result.Add(t);
            }
            return result;
        }
    }
}
