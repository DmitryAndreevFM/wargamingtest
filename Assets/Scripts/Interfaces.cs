﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public interface IPlane
    {
        SmoothMover Mover { get; }
        float Fuel { get; set; }
        void StartMission(List<ITarget> targets);
        void AbortMission();
        void AttachToAirport(IAirTrafficController airTrafficController);
        void TakeOff();
        void TouchDown(Transform transform);
        void KeepDistance(List<ITarget> targets, float distance);
        bool Landed { get; }
    }

    public interface IAirTrafficController
    {
        ITarget GetAirportTarget();
        bool TryLand(IPlane plane);
    }

    public interface ITarget
    {
        Vector2 Position { get; }
        Vector2 GetFuturePosition(float time);
        bool IsMoving { get; }
        bool IsRotating { get; }
    }

    public interface ISmoothMoveController
    {
        bool Enabled { get; set; }
        Vector2 ControlMovement(SmoothMover mover);
    }
}
