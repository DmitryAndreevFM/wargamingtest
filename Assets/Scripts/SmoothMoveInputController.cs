﻿using UnityEngine;

namespace Assets.Scripts
{
    public class SmoothMoveInputController : MonoBehaviour, ISmoothMoveController
    {
        public float Acceleration = 0.1f;
        public float AngleAcceleration = 0.1f;
        [Range(0f, 2f)]
        public float Impact = 1f;
        public float RotationImpact = 0.1f;

        private bool _enabled;
        public bool Enabled { get { return _enabled; } set { _enabled = value; enabled = _enabled; } }

        private bool _othersLocked;

        void Awake()
        {
            LockAllOtherControllers();
        }

        void Update()
        {
            // to avoid input loss - do it in update
            if (Input.GetKeyDown(KeyCode.A))
                LockAllOtherControllers(_othersLocked);
        }

#if UNITY_EDITOR
        // to be able to disable the component in runtime in the editor
        void OnEnable() { _enabled = true; }
        void OnDisable() { _enabled = false; }
#endif

        public Vector2 ControlMovement(SmoothMover mover)
        {
            Vector2 force = Vector2.zero;
            // warning: the following code is executed in FixedUpdate, so input loss is possible
            // todo: maybe capture input from regular Update and use it in this function
            if (Input.GetKey(KeyCode.UpArrow))
                force += mover.Forward * Impact;
            if (Input.GetKey(KeyCode.DownArrow))
                force -= mover.Forward * Impact;
            if (Input.GetKey(KeyCode.LeftArrow))
                force += mover.Left * RotationImpact;
            if (Input.GetKey(KeyCode.RightArrow))
                force -= mover.Left * RotationImpact;
            if (Input.GetKey(KeyCode.Space))
                // todo: it's a hack to make the final vector be aligned along the movement direction
                force = 1000000f * mover.Forward;
            return force;
        }

        private void LockAllOtherControllers(bool unlock = false)
        {
            _othersLocked = !unlock;
            ISmoothMoveController[] others = GetComponents<ISmoothMoveController>();
            for (int i = 0; i < others.Length; i++)
            {
                if (others[i] == this) continue;
                others[i].Enabled = unlock;
            }
        }
    }
}
