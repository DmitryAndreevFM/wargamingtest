﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class DistanceController : MonoBehaviour, ISmoothMoveController
    {
        private const int TargetsArrayInitialSize = 4;

        public float SteepFactor = 1f;
        [Range(0f, 2f)]
        public float Impact = 1f;

        // lists used instead of dictionaries and other structures to save memory and for fast iteration
        private readonly List<ITarget> _targets = new List<ITarget>(TargetsArrayInitialSize);
        private readonly List<float> _distances = new List<float>(TargetsArrayInitialSize);
        private readonly List<float> _persistences = new List<float>(TargetsArrayInitialSize);

        private bool _enabled;
        public bool Enabled { get { return _enabled; } set { _enabled = value; enabled = _enabled; } }

        void Awake()
        {
            _enabled = enabled;
        }

#if UNITY_EDITOR
        // to be able to disable the component in runtime in the editor
        void Update() { }
        void OnEnable() { _enabled = true; }
        void OnDisable() { _enabled = false; }
#endif

        #region target collections
        public void AddTarget(ITarget target, float desiredDistance, float persistence = 1f)
        {
            _targets.Add(target);
            _distances.Add(desiredDistance);
            _persistences.Add(persistence);
        }

        public void UpdateTarget(ITarget target, float desiredDistance, float persistence = 1f)
        {
            int indx = _targets.IndexOf(target);
            if (indx < 0)
                AddTarget(target, desiredDistance, persistence);
            else
            {
                _distances[indx] = desiredDistance;
                _persistences[indx] = persistence;
            }
        }

        public void ForgetTarget(ITarget target)
        {
            int indx = _targets.IndexOf(target);
            if (indx < 0)
                return;
            _targets.RemoveAt(indx);
            _distances.RemoveAt(indx);
            _persistences.RemoveAt(indx);
        }

        public void ForgetAllTargets()
        {
            _targets.Clear();
            _distances.Clear();
            _persistences.Clear();
        }
        #endregion

        public Vector2 ControlMovement(SmoothMover mover)
        {
            if (_targets.Count == 0)
                return Vector2.zero;
            // find result force from all the distance restrictions
            Vector2 force = new Vector2();
            for (int i = _targets.Count - 1; i >= 0; i--)
            {
                Vector2 toTarget = _targets[i].Position - mover.Position;
                float persistence = _persistences[i];
                float distance = toTarget.magnitude;
                float distanceInconsistence = distance - _distances[i];
                distanceInconsistence *= SteepFactor * persistence;
                force += (distanceInconsistence)*toTarget;
            }
            float magnitude = force.magnitude;
            if (magnitude > 1f)
                force /= magnitude;
            return force*Impact;
        }
    }
}
