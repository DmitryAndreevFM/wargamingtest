﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameObjectTarget : MonoBehaviour, ITarget
    {
        private Transform _transform;
        private Rigidbody _rigidbody;

        void Awake()
        {
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody>();
        }

        public Vector2 Position
        {
            get { return _transform.position; }
        }

        public Vector2 GetFuturePosition(float time)
        {
            if (_rigidbody == null)
                return _transform.position;
            return _transform.position + time*_rigidbody.velocity;
        }

        public bool IsMoving
        {
            get
            {
                if (_rigidbody == null)
                    return false;
                return _rigidbody.velocity.sqrMagnitude > float.Epsilon;
            }
        }

        public bool IsRotating
        {
            get { return false; }
        }
    }
}
